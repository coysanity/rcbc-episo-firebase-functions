export function addLeadingZero(walletId:number, places:number) {
    const zero = places - walletId.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + walletId;
}