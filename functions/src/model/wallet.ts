export interface walletModel {
    walletId: string,
    walletMaker: string,
    walletOwner: string,
    walletName: string,
    walletCurrency: string,
    walletBalance: number,
    walletType: string,
    walletStatus: string
}
