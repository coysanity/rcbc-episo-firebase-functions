export interface backOfficeUser {
    userId: string,
    Name: string,
    Department: string,
    Email: string,
    Password: string,
    ContactNo: string,
    Role: string,
    Status: string
}

export interface distributorUser {
    userId: string,
    firstName: string,
    middleName: string,
    lastName: string,
    nationality: string,
    birthDate: Date,
    birthPlace: string,
    contactNumber: string,
    address: string,
    businessName: string,
    natureOfWork: string,
    sourceOfFunds: string,
    email: string,
    password: string,
    status: string
}