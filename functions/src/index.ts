import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import * as moment from 'moment';
import * as cors from 'cors';

import  { backOfficeUser, distributorUser } from './model/user';
import { walletModel } from './model/wallet';
import { distributorPlan } from './model/plan';

import { addLeadingZero } from './helper/helper';

const app = express();
app.use(cors({ origin: true }));

admin.initializeApp();

const db = admin.database();
const auth = admin.auth();

/*******************************************************************************
*                              GET REQUEST                                    *
******************************************************************************/

app.get('/getBackOfficeUsers', (request, response) => {
    db.ref('Users').once('value')
        .then(snapshot => {
            const users = snapshot.val();
            const resultData = [];

            for (const user in users) {
                if (users[user]['Role'] !== 'Distributor' && users[user]['Role'] !== 'Subdistributor') {
                    const userObj = {
                        'Name': users[user]['Name'],
                        'UserID': users[user]['UserID'],
                        'DateCreation': users[user]['AccountDateCreation'],
                        'ContactNumber': users[user]['ContactNo'],
                        'Role': users[user]['Role'],
                        'Status': users[user]['Status']
                    };
                    resultData.push(userObj);
                }
            }
            response.send(resultData);
            
        }).catch(error => {
            response.status(500).send(error);
        }
    )
});

app.get('/getBackOfficeUsers/:userId', (request, response) => {
    const userId = request.params.userId;
    db.ref('Users/' + userId).once('value')
        .then(snapshot => {
            const user = snapshot.val();
            let resultObj = {};

            if (user === null) {
                resultObj = { Message: "User cannot be found" };
            } 
            else if (user['Role'] !== 'Distributor' && user['Role'] !== 'Subdistributor') {
                resultObj = {
                    Name: user['Name'],
                    UserID: user['UserID'],
                    DateCreation: user['AccountDateCreation'],
                    ContactNumber: user['ContactNo'],
                    Role: user['Role'],
                    Status: user['Status']
                };
            } 
            else {
                resultObj = { Message: "ID specified is not Back Office User" };
            }
            
            response.status(200).send(resultObj);
        }).catch(error => {
            response.status(500).send(error);
        }
    )
});

app.get('/getDistributorUsers', (request, response) => {

    db.ref('Users').once('value')
        .then(snapshot => {
            const users = snapshot.val();
            const resultData = [];

            for (const user in users) {
                if (users[user]['Role'] === 'Distributor') {
                    const userObj = {
                        Name: users[user]['Name'],
                        UserID: users[user]['UserID'],
                        DateCreation: users[user]['AccountDateCreation'],
                        BusinessName: users[user]['BusinessName'],
                        ContactNumber: users[user]['ContactNo'],
                        Address: users[user]['Address'],
                        Birthday: users[user]['Birthday'],
                        DistributorPlan: users[user]['DistributorPlan'],
                        EmailAddress: users[user]['Email'],
                        Nationality: users[user]['Nationality'],
                        NatureOfWork: users[user]['NatureOfWork'],
                        BirthPlace: users[user]['PlaceOfBirth'],
                        SourceFunds: users[user]['SourceOfFunds'],
                        Role: users[user]['Role'],
                        Status: users[user]['Status'],
                        Wallet: users[user]['Wallet']
                    };
                    resultData.push(userObj);
                }
            } 
            response.send(resultData);
        })
        .catch(error => {
            response.status(500).send(error);
        }
    )
});

app.get('/getDistributorUsers/:userId', (request, response) => {
    const userId = request.params.userId;

    db.ref('Users/' + userId).once('value')
        .then(snapshot => {
            const user = snapshot.val();
            let resultObj = {};

            if(user === null) {
                resultObj = { Message: "User cannot be found" };
            }
            else if(user['Role'] === 'Distributor') {
                resultObj = {
                    Name: user['Name'],
                    UserID: user['UserID'],
                    DateCreation: user['AccountDateCreation'],
                    BusinessName: user['BusinessName'],
                    ContactNumber: user['ContactNo'],
                    Address: user['Address'],
                    Birthday: user['Birthday'],
                    DistributorPlan: user['DistributorPlan'],
                    EmailAddress: user['Email'],
                    Nationality: user['Nationality'],
                    NatureOfWork: user['NatureOfWork'],
                    BirthPlace: user['PlaceOfBirth'],
                    SourceFunds: user['SourceOfFunds'],
                    Role: user['Role'],
                    Status: user['Status'],
                    Wallet: user['Wallet']
                };
            } else {
                resultObj = { Message: "ID specified is not Distributor User" };
            }

            response.status(200).send(resultObj);
        }).catch(error => {
            response.status(500).send(error);
        }
    )
});

app.get('/getDistributorPlan/:planId', (request, response) => {
    const planId = request.params.planId;

    db.ref('DistributorPlans/' + planId).once('value')
        .then(snapshot => {
            const distPlan = snapshot.val();
            let resultObj = {};

            if(distPlan === null) {
                resultObj = { Message: "Distributor plan cannot be found" };
            }
            else {
                resultObj = {
                    PlanName: distPlan['PlanName'],
                    SubDistDailyLimit: distPlan['SubDistDailyLimit'],
                    SubDistMonthlyLimit: distPlan['SubDistMonthlyLimit'],
                    AgentDailyLimit: distPlan['AgentDailyLimit'],
                    AgentMonthlyLimit: distPlan['AgentMonthlyLimit'],
                    DistDistributionFee: distPlan['DistDistributionFee'],
                    SubDistDistributionFee: distPlan['SubDistDistributionFee'],
                    SubDistPaySupplierFee: distPlan['SubDistPaySupplierFee'],
                    AgentDistributionFee: distPlan['AgentDistributionFee'],
                    AgentPaySupplierFee: distPlan['AgentPaySupplierFee'],
                };
            }
            response.status(200).send(resultObj);
        }).catch(error => {
            response.status(500).send(error);
        }
    )
})

app.get('/getWalletList', (request, response) => {

    db.ref('Wallet').once('value')
        .then(snapshot => {
            const wallets = snapshot.val();
            const resultData = [];

            for (const wallet in wallets) {
                const walletObj = {
                    AccountId: wallets[wallet]['account_id'],
                    WalletName: wallets[wallet]['wallet_name'],
                    Currency: wallets[wallet]['currency'],
                    WalletType: wallets[wallet]['wallet_type'],
                    DistName: wallets[wallet]['dist_id'],
                    WalletStatus: wallets[wallet]['wallet_status'],
                    CreatedBy: wallets[wallet]['created_by'],
                    DateCreated: wallets[wallet]['datetime_created']
                };
                resultData.push(walletObj);
            }
            response.send(resultData);
        }).catch(error => {
            response.status(500).send(error);
        })
});

app.get('/getWalletTransaction', (request, response) => {

    db.ref('Wallet').once('value')
        .then(snapshot => {
            const wallets = snapshot.val();
            const resultData = [];

            for (const wallet in wallets) {
                const walletObj = {
                    AccountId: wallets[wallet]['account_id'],
                    Balance: wallets[wallet]['balance'],
                    Transactions: wallets[wallet]['Transactions']
                };
                resultData.push(walletObj);
            }
            response.send(resultData);
        }).catch(error => {
            response.status(500).send(error);
        })

});

app.get('/getCurrentWalletId', (request, response) => {

    db.ref('Wallet').once('value')
        .then(snapshot =>{
            const walletIds = snapshot.val();
            const currentYear = moment().year();
            const walletList = [];

            for(const id in walletIds) {
                const walletKey = id.split("-");

                if(parseInt(walletKey[1]) == currentYear) {
                    walletList.push(parseInt(walletKey[2]));
                }
            }

            const lastId = Math.max.apply(Math, walletList);
            const currentId = "ACC-" + currentYear + "-" + addLeadingZero((lastId+1),7);

            response.status(500).send({CurrentWalletId: currentId});

        }).catch(error => {
            response.status(500).send(error);
        }
    );
})

app.get('/getPendingDistPlan', (request, response) => {
    db.ref('DistributorPlans').once('value')
        .then(snapshot => {
            const distPlans = snapshot.val();
            const pendingPlans = []

            for(const id in distPlans) {
                if(distPlans[id]['NewPlanName']){
                    pendingPlans.push(distPlans[id]);
                }
            }
            response.status(200).send(pendingPlans);
        })
        .catch(error => {
            response.status(500).send(error);
        })
})

app.get('/getPendingDistPlan/:planId', (request, response) => {
    const planId = request.params.planId;
    db.ref('DistributorPlans').child(planId).once('value')
        .then(snapshot => {
            const distPlan = snapshot.val();

            if (distPlan['NewPlanName']) {
                response.status(200).send(distPlan);
            }
            else {
                response.status(200).send({Message: "No changes for this distributor plan"});
            }
        })
        .catch(error => {
            response.status(500).send(error);
        })
})


/*******************************************************************************
*                              POST REQUEST                                   *
******************************************************************************/

app.post('/createBackOfficeUser', (request, response) => {
    const {
        Name,
        Department,
        Email,
        Password,
        ContactNo,
        Role,
    }: backOfficeUser = request.body;

    auth.createUser({
        email: Email,
        emailVerified: false,
        password: Password,
        displayName: Name,
        disabled: false
    }).then(userSnapshot => {
        const currentDate = moment().format('MM-DD-YYYY');

        db.ref('Users').child(userSnapshot.uid).set({
            Name: Name,
            Department: Department,
            ContactNo: ContactNo,
            Email: Email,
            Role: Role,
            UserID: userSnapshot.uid,
            PasswordStatus: "default",
            AccountDateCreation: currentDate
        }).then(success => {
            response.status(200).send({Message: "Successfully Created User"});
        }).catch(error => {
            response.status(500).send(error);
        });

    }).catch(error => {
        response.status(500).send(error);
    });
})

app.post('/updateBackOfficeUser', (request, response) => {
    const {
        userId,
        Name,
        Department,
        Email,
        ContactNo,
        Status,
    }: backOfficeUser = request.body;

    if(userId) {

        db.ref('Users').child(userId).once('value')
            .then(snapshot => {
                const user = snapshot.val();
                return user;
            }).then(snapshot => {
                const user = snapshot;

                if(user === null) {
                    response.status(404).send({Message: "User cannot be found"});
                }
                else if(user['Role'] !== 'Distributor' && user['Role'] !== 'Subdistributor') {
                    const boId = user['UserID'];

                    db.ref('Users').child(boId).update({
                        Name: Name,
                        Department: Department,
                        Email: Email,
                        ContactNo: ContactNo,
                        Status: Status
                    }).then(success => {
                        response.status(200).send({Message: "Successfully Updated User"});
                    }).catch(error => {
                        console.error(error);
                    });
                }
                else {
                    response.status(404).send({Message: "ID specified is not Back Office User"});
                }
            }).catch(error => {
                response.status(500).send(error)
            }
        );
    }
    else {
        response.status(500).send({Message: "User's ID required"});
    }

})

app.post('/createDistributorUser', (request, response) => {
    const {
        firstName,
        middleName,
        lastName,
        nationality,
        birthDate,
        birthPlace,
        contactNumber,
        address,
        businessName,
        natureOfWork,
        sourceOfFunds,
        email,
        password
    }: distributorUser = request.body;

    const fullName = firstName + " " + middleName + " " + lastName
 
    auth.createUser({
        email: email,
        emailVerified: false,
        password: password,
        displayName: fullName,
        disabled: false
    }).then(userSnapshot => {

        const currentDate = moment().format('MM-DD-YYYY');
        
        db.ref('Users').child(userSnapshot.uid).set({
            AccountDateCreation: currentDate,
            UserID: userSnapshot.uid,
            FirstName: firstName,
            MiddleName: middleName,
            LastName: lastName,
            Email: email,
            Nationality: nationality,
            BirthDate: birthDate,
            BirthPlace: birthPlace,
            ContactNumber: contactNumber,
            Address: address,
            BusinessName: businessName,
            NatureOfWork: natureOfWork,
            SourceOfFunds: sourceOfFunds,
            Role: "Distributor", 
            Status: "Active"
        }).then(success => {
            response.status(200).send({Message: "Successfully Created User"});
        }).catch(error => {
            response.status(500).send(error);
        });
    }).catch(error => {
        response.status(500).send(error);
    });
    
})

app.post('/updateDistributorUser', (request, response) => {
    const {
        userId,
        firstName,
        middleName,
        lastName,
        nationality,
        birthDate,
        birthPlace,
        contactNumber,
        address,
        businessName,
        natureOfWork,
        sourceOfFunds,
        email,
        status
    }: distributorUser = request.body;

    if (userId) {
        db.ref('Users').child(userId).once('value')
        .then(snapshot => {
            const user= snapshot.val();
    
            if(user === null){
                response.status(404).send({Message: "User cannot be found"});
            }
            else if(user['Role'] === 'Distributor') {
                const distId = user['UserID'];
 
                db.ref('Users').child(distId).update({
                    FirstName: firstName,
                    MiddleName: middleName,
                    LastName: lastName,
                    Email: email,
                    Nationality: nationality,
                    BirthDate: birthDate,
                    BirthPlace: birthPlace,
                    ContactNumber: contactNumber,
                    Address: address,
                    BusinessName: businessName,
                    NatureOfWork: natureOfWork,
                    SourceOfFunds: sourceOfFunds,
                    Status: status
                }).then(success => {
                    response.status(200).send({Message: "Successfully Updated User"});
                }).catch(error => {
                    console.error(error);
                });
            }
            else {
                response.status(404).send({ Message: "ID specified is not Distributor User" });
            }
        }).catch(error => {
            response.status(500).send(error);
        });
    }
    else {
        response.status(500).send({Message: "User's ID required"});
    }

})

app.post('/createWallet', (request, response) => {

    const {
        walletMaker,
        walletOwner,
        walletName,
        walletCurrency,
        walletBalance,
        walletType
    }: walletModel = request.body;

    const currentDate = moment().format('MM-DD-YYYY');

    db.ref('Wallet').once('value')
        .then(snapshot =>{
            const walletIds = snapshot.val();
            const currentYear = moment().year();
            const walletList = [];

            for(const id in walletIds) {
                const walletKey = id.split("-");

                if(parseInt(walletKey[1]) == currentYear) {
                    walletList.push(parseInt(walletKey[2]));
                }
            }

            const lastId = Math.max.apply(Math, walletList);
            return "ACC-" + currentYear + "-" + addLeadingZero((lastId+1),7);
        }).then(currentId => {
            const walletId = currentId;

            db.ref('Wallet').child(walletId).set({
                AccountDateCreation: currentDate,
                WalletId: walletId,
                WalletName: walletName,
                WalletType: walletType,
                WalletStatus: "Active",
                Balance: walletBalance,
                CreatedBy: walletMaker,
                OwnedBy: walletOwner,
                Currency: walletCurrency,
            }).then(success => {
        
                db.ref('Users/' + walletOwner + "/Wallet").push().set({
                    WalletId: walletId
                }).then(snapshot => {
                    response.status(200).send({Message: "Successfully Created Wallet"});
                }).catch(error => {
                    response.status(500).send(error)
                });
        
            }).catch(error => {
                response.status(500).send(error);
            });

        }).catch(error => {
            response.status(500).send(error);
        }
    );

})

app.post('/updateWallet', (request, response) => {
    const {
        walletId,
        walletName,
        walletStatus
    }: walletModel = request.body;

    if (walletId) {
        db.ref('Wallet').child(walletId).once('value')
            .then(snapshot => {
                const walletInfo = snapshot.val();
                return walletInfo;
            }).then(snapshot => {
                const wallet = snapshot;
            
                if (wallet) {
                    const id = snapshot['WalletId'];

                    db.ref('Wallet').child(id).update({
                        WalletName: walletName,
                        WalletStatus: walletStatus
                    }).then(success => {
                        response.status(200).send({Message: "Successfully Updated Wallet"});
                    }).catch(error => {
                        console.error(error);
                    });
                }
                else {
                    response.status(404).send({Message: "Wallet ID not registered in the database"});
                }
            }).catch(error => {
                response.status(500).send({Message: "Internal Error"});
            });
    }
    else {
        response.status(500).send({Message: "User's ID required"});
    }

})

app.post('/createDistPlan', (request, response) => {
    const {
        PlanName,
        SubDistDailyLimit,
        SubDistMonthlyLimit,
        AgentDailyLimit,
        AgentMonthlyLimit,
        DistDistributionFee,
        SubDistDistributionFee,
        SubDistPaySupplierFee,
        AgentDistributionFee,
        AgentPaySupplierFee,
        Added_By
    }: distributorPlan = request.body;

    const currentDate = moment().format('MM-DD-YYYY HH:mm:ss');

    db.ref('DistributorPlans').once('value')
        .then(snapshot => {
            const distplans = snapshot.val()
            const planIds = [];

            for (const plan in distplans){
                const planKey = plan.split("-");
                planIds.push(parseInt(planKey[1]));
            }

            const lastId = Math.max.apply(Math, planIds);
            return "PLAN-" + addLeadingZero((lastId+1),7)
        })
        .then(snapshot => {
            const distPlanId = snapshot;

            db.ref('DistributorPlans').child(distPlanId).set({
                PlanID: distPlanId,
                PlanName: PlanName,
                SubDistDailyLimit: SubDistDailyLimit,
                SubDistMonthlyLimit: SubDistMonthlyLimit,
                AgentDailyLimit: AgentDailyLimit,
                AgentMonthlyLimit: AgentMonthlyLimit,
                DistDistributionFee: DistDistributionFee,
                SubDistDistributionFee: SubDistDistributionFee,
                SubDistPaySupplierFee: SubDistPaySupplierFee,
                AgentDistributionFee: AgentDistributionFee,
                AgentPaySupplierFee: AgentPaySupplierFee,
                Status: "Inactive",
                NewPlanName: "",
                NewSubDistDailyLimit: "",
                NewSubDistMonthlyLimit: "",
                NewAgentDailyLimit: "",
                NewAgentMonthlyLimit: "",
                NewDistDistributionFee: "",
                NewSubDistDistributionFee: "",
                NewSubDistPaySupplierFee: "",
                NewAgentDistributionFee: "",
                NewAgentPaySupplierFee: "",
                NewStatus: "",
                AccountDateCreation: currentDate,
                ApprovalStatus: "Pending",
                Added_By: Added_By
            }).then(success => {
                response.status(200).send({Message: `Successfully Updated User: ${distPlanId}` });
            }).catch(error => {
                console.error(error);
            })
        })
        .catch(error => {
            response.status(500).send({Message: "Internal Error"});
        });

})

app.put('/updateDistPlan/:planId', (request, response) => {
    const planId = request.params.planId;
    const {
        NewPlanName,
        NewSubDistDailyLimit,
        NewSubDistMonthlyLimit,
        NewAgentDailyLimit,
        NewAgentMonthlyLimit,
        NewDistDistributionFee,
        NewSubDistDistributionFee,
        NewSubDistPaySupplierFee,
        NewAgentDistributionFee,
        NewAgentPaySupplierFee
    }: distributorPlan = request.body;

    if(planId) {
        db.ref('DistributorPlans').child(planId).update({
            NewPlanName: NewPlanName,
            NewSubDistDailyLimit: NewSubDistDailyLimit,
            NewSubDistMonthlyLimit: NewSubDistMonthlyLimit,
            NewAgentDailyLimit: NewAgentDailyLimit,
            NewAgentMonthlyLimit: NewAgentMonthlyLimit,
            NewDistDistributionFee: NewDistDistributionFee,
            NewSubDistDistributionFee: NewSubDistDistributionFee,
            NewSubDistPaySupplierFee: NewSubDistPaySupplierFee,
            NewAgentDistributionFee: NewAgentDistributionFee,
            NewAgentPaySupplierFee: NewAgentPaySupplierFee
        }).then(snapshot => {
            response.status(404).send({ Message: `Success updating the distriburtor plan ${planId}`});
        }).catch(error => {
            response.status(404).send({ Message: `Error updating the distriburtor plan ${planId}`});
        });
    }
    else {
        response.status(404).send({ Message: "Plan ID is not specified" });
    }

})

app.put('/updateDistPlanStatus/:planId', (request, response) => {
    const planId = request.params.planId;
    const {
        Status,
        ApprovalStatus
    }: distributorPlan = request.body;

    if(planId){
        db.ref('DistributorPlans').child(planId).update({
            Status: Status,
            ApprovalStatus: ApprovalStatus
        }).then(snapshot => {
            response.status(404).send({ Message: `Success updating status of the distriburtor plan ${planId}`});
        }).catch(error => {
            response.status(404).send({ Message: `Error updating status of the distriburtor plan ${planId}`});
        });
    }
    else{
        response.status(404).send({ Message: "Plan ID is not specified" });
    }
})

app.put('/approveDistPlan/:planId', (request, response) => {
    const planId = request.params.planId;

    if(planId) {
        db.ref('DistributorPlans').child(planId).once('value')
            .then(snapshot => {
                const distPlan = snapshot.val();
                return distPlan
            })
            .then(snapshot => {
                const distPlan = snapshot;

                if (distPlan['NewPlanName']) {
                    db.ref('DistributorPlans').child(planId).update({
                        PlanName: distPlan['NewPlanName'],
                        SubDistDailyLimit: distPlan['NewSubDistDailyLimit'],
                        SubDistMonthlyLimit: distPlan['NewSubDistMonthlyLimit'],
                        AgentDailyLimit: distPlan['NewAgentDailyLimit'],
                        AgentMonthlyLimit: distPlan['NewAgentMonthlyLimit'],
                        DistDistributionFee: distPlan['NewDistDistributionFee'],
                        SubDistDistributionFee: distPlan['NewSubDistDistributionFee'],
                        SubDistPaySupplierFee: distPlan['NewSubDistPaySupplierFee'],
                        AgentDistributionFee: distPlan['NewAgentDistributionFee'],
                        AgentPaySupplierFee: distPlan['NewAgentPaySupplierFee'],
                        NewPlanName: '',
                        NewSubDistDailyLimit: '',
                        NewSubDistMonthlyLimit: '',
                        NewAgentDailyLimit: '',
                        NewAgentMonthlyLimit: '',
                        NewDistDistributionFee: '',
                        NewSubDistDistributionFee: '',
                        NewSubDistPaySupplierFee: '',
                        NewAgentDistributionFee: '',
                        NewAgentPaySupplierFee: ''
                    }).then(success => { 
                        response.status(200).send({Message: `Successfully approved the changes for Distributor Plan: ${planId}`});
                    }).catch(error => {
                        response.status(500).send(error);
                    })
                }
                else {
                    response.status(200).send({Message: "Approval cannot be execute, there is no changes in this distributor plan"});
                } 
            })
            .catch(error => {
                response.status(500).send(error);
            })

    }
    else {
        response.status(404).send({ Message: "Plan ID is not specified" });
    }
})

app.put('/disapproveDistPlan/:planId', (request, response) => {
    const planId = request.params.planId;

    if(planId) {
        db.ref('DistributorPlans').child(planId).update({
            NewPlanName: '',
            NewSubDistDailyLimit: '',
            NewSubDistMonthlyLimit: '',
            NewAgentDailyLimit: '',
            NewAgentMonthlyLimit: '',
            NewDistDistributionFee: '',
            NewSubDistDistributionFee: '',
            NewSubDistPaySupplierFee: '',
            NewAgentDistributionFee: '',
            NewAgentPaySupplierFee: ''
        }).then(snapshot => {
            response.status(200).send({Message: `Successfully disapproved the changes for Distributor Plan: ${planId}`});
        }).catch(error => {
            response.status(500).send(error);
        })
    }
    else {
        response.status(404).send({ Message: "Plan ID is not specified" });
    }
})

exports.calls = functions.https.onRequest(app);